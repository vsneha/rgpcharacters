﻿using RPGCharacters.Aspects;
using RPGCharacters.weapon;
using System;

namespace RPGCharacters.Armor

{

    public class ArmorAttributes : Item
    {
        public ArmorAttributes ArmorType { get; set; }
        public Slots ArmorSlot { get; set; }
        public PrimaryAttributes ArmorAttributesofPrimary { get; set; }
        public PrimaryAttributes PrimaryAttributes { get; set; }
        
    }
}