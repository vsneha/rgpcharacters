﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacters.Aspects
{
    public enum Weapons
    {
        Axe,
        Bows,
        Daggers,
        Hammers,
        Staffs,
        Swords,
        Wands

    }

    public enum Armors
    {
        Cloth,
        Leather,
        Mail,
        Plate
    }

    public enum Slots
    {
        Head,
        Body,
        Legs,
        Weapon
    }
}


